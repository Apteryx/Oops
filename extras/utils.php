<?php

include("sql_info.php");
/*
Author: Apteryx
Date: 7/13/2016
Time: 4:12 PM
Description: Utilities for internal functions and file manipulation.
*/

class Utils {



  function generateString($length) {
      $generatedString = "";
      $selection = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
      for ($i = 0; $i < $length; $i++){
          $generatedString .= $selection[mt_rand(0, strlen($selection) - 1)];
      }
      return $generatedString;
  }

  /*
  getUsername:
    key: The identification key.

    Gets the username from the key. Returns a string.
  */

  function getUsername($key) {
    global $mysql_host, $mysql_username, $mysql_password, $mysql_database;
    $sql = new mysqli($mysql_host, $mysql_username, $mysql_password, $mysql_database);
    if ($sql->connect_errno > 0) {
      echo "SQL error: getUsername...";
    }

    $query = $sql->query("SELECT * FROM users");
    if (!$query) {
      echo $sql->error;
      return "SQL Error: getUsername... (36)";
    }
    $sql->close();
    while($rows = $query->fetch_assoc()) {
      if ($rows['key'] == $key) {
        return $rows['user'];
      }
    }
  }

  /*
  validateKey:
    key: The indentification key.

    Validates the key against the database. Returns a boolean.
  */

  function validateKey($key) {
    //echo "Validate key function\n";
    global $mysql_host, $mysql_username, $mysql_password, $mysql_database;
    $sql = new mysqli($mysql_host, $mysql_username, $mysql_password, $mysql_database);
    //echo "After SQL creation\n";
    $result = $sql->query("SELECT * FROM users");
    if ($sql->connect_errno > 0) {
      echo "SQL error: validateKey...";
    }
    $sql->close();
    while($rows = $result->fetch_assoc()) {
      if ($rows['key'] == $key) {
        //echo "true";
        return true;
      }
    }
    return false;
  }

  /*
  handleDeletion:
    dkey: Deletion key generated when a file is uploaded.

    Deletes the file that matches the same row as the deletion key.
  */

  function handleDeletion($dkey) {
    global $mysql_host, $mysql_username, $mysql_password, $mysql_database;
    $sql = new mysqli($mysql_host, $mysql_username, $mysql_password, $mysql_database);
    if ($sql->connect_errno > 0) {
      echo "SQL error: handleDeletion...";
    }
      $query = $sql->query("SELECT * FROM 'logs'");
      if (!query) {
        echo $sql->error;
      }
      $sql->close();
      while($rows = $query->fetch_assoc()) {
        if($rows['deletion'] == $dkey) {
          unlink($rows['file']);
          echo "File deleted: " . $rows['file'];
        }
      }
    }



  /*
  renameFile:
    file: The file name.
    name: The new name for the file.
  */

  function renameFile($file, $name) {
    $name = getcwd() . "/" . $name;
    move_uploaded_file($file, $name);
  }




  function checkFile($extension) {
    $blocked_extensions = array (
  		"js",
  		"php",
  		"php4",
  		"php3",
  		"phtml",
  		"rhtml",
  		"html",
  		"html",
  		"xhtml",
  		"jhtml",
  		"css",
  		"swf"
    );

    foreach($blocked_extensions as $bextension) {
      if ($bextension == $extension) {
        return false;
      }
    }
    return true;
  }

  /*
  handleUpload:
    key: The identification key.
    file: The uploaded file.
    uploaderAddress: The IP address of the uploader.

    Handles the uploading for the files
  */

  function handleUpload($key, $ip, $file, $method) {
    //echo "Fisted.\n";
      if ($this->validateKey($key)) {
        global $mysql_host, $mysql_username, $mysql_password, $mysql_database;
        $extension = explode(".", $file['name']);
        //echo $extension[0]."\n";
        if ($extension[1]) {
          $final = $this->generateString(mt_rand(4, 8));
          //echo "First final.\n";
          $dkey = $this->generateString(25);
          //echo "DKey.\n";
          $final .= "." . $extension[1];
          //echo "Second final.\n";
          $this->renameFile($file['tmp_name'], $final);
          //echo "Rename file.\n";
            switch($method) {
              case"plaintext":
              echo "http://oops.moe/" . $final . ":" . $dkey;
                break;
              case"json":
              echo json_encode(array('filename' => $final, 'deletion' => $dkey));
                break;
            }
          $sql = new mysqli($mysql_host, $mysql_username, $mysql_password, $mysql_database);
          $final = $sql->real_escape_string($final);
          $ip = $sql->real_escape_string($ip);
          $sql->query("INSERT INTO `logs` (`user`, `key`, `filename`, `deletion`, `ip`) VALUES ('". $this->getUsername($key) ."', '". $key ."', '". $final ."','". $dkey ."', '". $ip ."')");
          $sql->close();
        }else{
          echo "Invalid file type.";
        }
      }

  }



}



?>
