<?php
include("extras/utils.php");
/*
Author: Apteryx
Date: 7/13/2016
Time: 4:01 PM
Description: Simple upload script.

In order to use this you need to create a sql_info.php file with the varibles:
$mysql_host
$mysql_database
$mysql_username
$mysql_password

Set to all the varibles that you need to make an SQL connection.
*/

$utils = new Utils();

if (isset($_POST['key']) && isset($_FILES['file'])) {
  $key = $_POST['key'];
  $file = $_FILES['file'];
  $ip = $_SERVER['HTTP_CF_CONNECTING_IP'];
  if (isset($_POST['method'])) {
    $method = $_POST['method'];
  }else{
    $method = "plaintext";
    //echo "Plaintext\n";
  }
  $utils->handleUpload($key, $ip, $file, $method);
} else {
  echo "You're missing some arguments, consult the documentation.";
}

?>
