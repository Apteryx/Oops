<?php
include("extras/utils.php");
/*
Author: Apteryx
Date: 7/17/2016
Time: 4:45 AM
Description: Deletes a file on the server that matches the key in the logs with the key provided.
*/

$utils = new Utils();

if (isset($_POST['delete'])) {
  $dkey = $_POST['delete'];
  $utils->handleDeletion($dkey);
} else {
  echo "You're missing some arguments, consult the documentation.";
}


?>
